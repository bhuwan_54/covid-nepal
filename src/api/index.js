import axios from "axios";

const url = "https://corona.askbhunte.com/api/v1/data/nepal";

export const fetchCardData = async () => {
  try {
    const {
      data: { tested_positive, recovered, deaths, updated_at,tested_total,tested_negative, },
    } = await axios.get(url);

    return { tested_positive, recovered, deaths, updated_at,tested_total,tested_negative };
  } catch (error) {
    return error;
  }
};


