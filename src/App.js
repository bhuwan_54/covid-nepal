import React from "react";

import { Cards,  Chart } from "./components";
import {  fetchCardData } from "./api/";
import styles from "./App.module.css";

import image from "./images/image.png";

class App extends React.Component {
  state = {
    data: {},
  };

  async componentDidMount() {

    const data = await fetchCardData();

    this.setState({ data });
  }

  

  render() {
    const { data } = this.state;

    return (
      <div className={styles.container}>
        <img className={styles.image} src={image} alt="COVID-19" />
        <Cards data={data} />
        <Chart data={data}  />
      </div>
    );
  }
}

export default App;
