import React from 'react';
import { Typography, Grid } from '@material-ui/core';
import CardComponent from './Card/Card';
import styles from './Cards.module.css';

const Info = ({ data: { tested_positive, recovered, deaths, updated_at } }) => {
  if (!tested_positive) {
    return 'Loading...';
  }

  return (
    <div className={styles.container}>
        <Typography gutterBottom variant="h4" component="h2">Covid-19 Nepal</Typography>
      <Grid container spacing={3} justify="center">
        <CardComponent
          className={styles.infected}
          cardTitle="Infected"
          value={tested_positive}
          lastUpdate={updated_at}
          cardSubtitle="Number of active cases from COVID-19."
        />
        <CardComponent
          className={styles.recovered}
          cardTitle="Recovered"
          value={recovered}
          lastUpdate={updated_at}
          cardSubtitle="Number of recoveries from COVID-19."
        />
        <CardComponent
          className={styles.deaths}
          cardTitle="Deaths"
          value={deaths}
          lastUpdate={updated_at}
          cardSubtitle="Number of deaths caused by COVID-19."
        />
        
      </Grid>
    </div>
  );
};

export default Info;
