import React   from 'react';
import {  Bar } from 'react-chartjs-2';
import styles from './Chart.module.css';

const Chart = ({ data: { tested_positive, tested_total, tested_negative } }) => {

  
  const barChart = (
    tested_positive ? (
      <Bar
        data={{
          labels: ['Total Test Done', 'Tested Negative', 'Positive/Infected'],
          datasets: [
            {
              label: 'People',
              backgroundColor: ['yellow', 'green', 'blue'],
              data: [tested_total, tested_negative, tested_positive],
            },
          ],
        }}
        options={{
          legend: { display: false },
          title: { display: true, text: `Current scenario in Nepal` },
        }}
      />
    ) : null
  );


  return (
    <div className={styles.container}>
      { barChart}
    </div>
  );
};

export default Chart;
